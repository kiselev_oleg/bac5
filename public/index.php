<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_COOKIE['save'])&&empty($_GET['change'])) {
      include('success.php');

      exit();
  }

  $values = array();

  $errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['gender'] = !empty($_COOKIE['gender_error']);
  $errors['numOfLimbs'] = !empty($_COOKIE['numOfLimbs_error']);
  $errors['superpowers'] = !empty($_COOKIE['suoerpowers_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['checkbox'] = !empty($_COOKIE['checkbox_error']);

  if ($errors['name']) $errors['name']='необходимо ввести корректное имя';
  if (!empty($_COOKIE['name_error'])&&$_COOKIE['name_error']=='2') $errors['name']=$errors['name'].'<br>разрешены только A-Z a-z 0-9 _ а-я А-Я';
  if ($errors['email']) $errors['email']='необходимо ввести корректный email';
  if (!empty($_COOKIE['email_error'])&&$_COOKIE['email_error']=='2') $errors['email']=$errors['email'].'<br>разрешены только A-Z a-z 0-9 _ . @';
  if ($errors['date']) $errors['date']='необходимо ввести корректную дату';
  if ($errors['gender']) $errors['gender']='необходимо выбрать пол';
  if ($errors['numOfLimbs']) $errors['numOfLimbs']='необходимо выбрать количество конечностей';
  if ($errors['superpowers']) $errors['superpowers']='необходимо выбрать правильные суперспособности';
  if ($errors['biography']) $errors['biography']='необходимо ввести биографию';
  if (!empty($_COOKIE['biography_error'])&&$_COOKIE['biography_error']=='2') $errors['biography']=$errors['biography'].'<br>разрешены только A-Z a-z 0-9 _ . , : ! ? * / - + а-я А-Я';
  if ($errors['checkbox']) $errors['checkbox']='необходимо согласие';

  $values = array();
  if(!empty($_COOKIE['name_value'])) $values['name']=$_COOKIE['name_value'];
  if(!empty($_COOKIE['email_value'])) $values['email']=$_COOKIE['email_value'];
  if(!empty($_COOKIE['date_value'])) $values['date']=$_COOKIE['date_value'];
  if(!empty($_COOKIE['gender_value'])) $values['gender']=$_COOKIE['gender_value'];
  if(!empty($_COOKIE['numOfLimbs_value'])) $values['numOfLimbs']=$_COOKIE['numOfLimbs_value'];
  /*if(!empty($_COOKIE['superpowers_value'])) $values['superpowers']=$_COOKIE['superpowers_value'];*/
  if(!empty($_COOKIE['superpowers_value_3'])) $values['superpowers_3']=1;
  if(!empty($_COOKIE['superpowers_value_1'])) $values['superpowers_1']=1;
  if(!empty($_COOKIE['superpowers_value_2'])) $values['superpowers_2']=1;
  if(!empty($_COOKIE['biography_value'])) $values['biography']=$_COOKIE['biography_value'];
  if(!empty($_COOKIE['checkbox_value'])) $values['checkbox']=$_COOKIE['checkbox_value'];

  include('form.php');
}
else {
  setcookie('name_error', '', 100000);
  setcookie('email_error', '', 100000);
  setcookie('date_error', '', 100000);
  setcookie('gender_error', '', 100000);
  setcookie('numOfLimbs_error', '', 100000);
  setcookie('superpowers_error', '', 100000);
  setcookie('biography_error', '', 100000);
  setcookie('checkbox_error', '', 100000);
  
  setcookie('name_value', '', 100000);
  setcookie('email_value', '', 100000);
  setcookie('date_value', '', 100000);
  setcookie('gender_value', '', 100000);
  setcookie('numOfLimbs_value', '', 100000);
  setcookie('superpowers_value_1', '', 100000);
  setcookie('superpowers_value_2', '', 100000);
  setcookie('superpowers_value_3', '', 100000);
  setcookie('biography_value', '', 100000);
  setcookie('checkbox_value', '', 100000);
  
  $errors = FALSE;
  if (empty($_POST['name'])) {
    setcookie('name_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else if(!preg_match("/^[A-Za-z0-9_а-яА-Я]+$/", $_POST['name'])) {
    setcookie('name_error', '2', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  if (empty($_POST['email'])) {
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else if(!preg_match("/^[\w\.\d]+@\w+\.\w\w\w?$/", $_POST['email'])) {
    setcookie('email_error', '2', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  if (empty($_POST['date'])) {
    setcookie('date_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  if (empty($_POST['gender'])) {
    setcookie('gender_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  if (empty($_POST['numOfLimbs'])) {
    setcookie('numOfLimbs_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  if (empty($_POST['biography'])) {
    setcookie('biography_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else if(!preg_match("/^[A-Za-z0-9_\.\s,:!\?\*-\+а-яА-Я]+$/", $_POST['biography'])) {
    setcookie('biography_error', '2', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  if(empty($_POST['checkbox'])||$_POST['checkbox']==FALSE) {
    setcookie('checkbox_error','1',time()+24*60*60);
    $errors=true;
  }

  if (!empty($_POST['name'])) {
    setcookie('name_value', $_POST['name'], time() + 24 * 60 * 60);
  }
  if (!empty($_POST['email'])) {
    setcookie('email_value', $_POST['email'], time() + 24 * 60 * 60);
  }
  if (!empty($_POST['date'])) {
    setcookie('date_value', $_POST['date'], time() + 24 * 60 * 60);
  }
  if (!empty($_POST['gender'])) {
    setcookie('gender_value', $_POST['gender'], time() + 24 * 60 * 60);
  }
  if (!empty($_POST['numOfLimbs'])) {
    setcookie('numOfLimbs_value', $_POST['numOfLimbs'], time() + 24 * 60 * 60);
  }
  /*if (!empty($_POST['superpowers'])) {
    setcookie('superpowers_value', $_POST['superpowers'], time() + 24 * 60 * 60);
  }*/
  foreach($_POST['superpowers'] as $type) {
    if($type=='бессмертие') $type='1';
    if($type=='хождение сквозь стены') $type='2';
    if($type=='левитация') $type='3';
    setcookie('superpowers_value_'.$type,1,time()+24*60*60);
  }
  if (!empty($_POST['biography'])) {
    setcookie('biography_value', $_POST['biography'], time() + 24 * 60 * 60);
  }
  if (!empty($_POST['checkbox'])&&$_POST['checkbox']==true) {
    setcookie('checkbox_value', 'checked', time() + 24 * 60 * 60);
  }

  if ($errors) {
    header('Location: index.php?change=1');
    exit();
  }

      $log;
      $password;
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
      $user = 'u20969';
      $pass = '8283569';
      
      $db = new PDO('mysql:host=localhost;dbname=u20969', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

      $stmt = $db->prepare("delete from bac3_list_superpower where id_person=(select id_person from bac5_log where log='".$_SESSION['login']."')");
      $stmt -> execute(array());
      
      $stmt = $db->prepare("UPDATE bac3_person SET name='".$_POST['name']."',e_mail='".$_POST['email']."',year_of_birth=str_to_date('".$_POST['date']."','%Y-%c-%d'),gender=".$_POST['gender']."-1,num_limbs=".$_POST['numOfLimbs'].",biografy='".$_POST['biography']."' where id=(select id_person from bac5_log where log='".$_SESSION['login']."')");
      $stmt -> execute(array());
      foreach($_POST['superpowers'] as $type) {
        $stmt = $db->prepare("INSERT INTO `bac3_list_superpower` (id_person,id_superpower) VALUES ((select id_person from bac5_log where log='".$_SESSION['login']."'),(select id from bac3_superpower where type='".$type."'))");
        $stmt -> execute(array());
      }
  } else {
  try {
      $log;
      $password;

      $user = 'u20969';
      $pass = '8283569';
      $db = new PDO('mysql:host=localhost;dbname=u20969', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
      $stmt = $db->prepare("INSERT INTO bac3_person (name,e_mail,year_of_birth,gender,num_limbs,biografy) VALUES ('".$_POST['name']."','".$_POST['email']."',str_to_date('".$_POST['date']."','%Y-%c-%d'),".$_POST['gender']."-1,".$_POST['numOfLimbs'].",'".$_POST['biography']."')");
      $stmt -> execute(array());
  
      foreach($_POST['superpowers'] as $type) {
        $stmt = $db->prepare("INSERT INTO `bac3_list_superpower` (id_person,id_superpower) VALUES ((select max(id) from bac3_person),(select id from bac3_superpower where type='".$type."'))");
        $stmt -> execute(array());
      }

      $log=($db->query("select max(id) from bac5_log")->fetchAll(PDO::FETCH_COLUMN)[0]);
      //$password='pas'.$log.'qori5ng';
      $password=substr(str_shuffle((string)time()),0,10);
      $log='log'.$log;

      $stmt = $db->prepare("INSERT INTO bac5_log (id_person,log,password) VALUES ((select id from bac3_person where name='".$_POST['name']."'),'".$log."','".md5(md5($password.$log).'12937463sdfjbkldfghjb!iu4ytro9huo4938htiuhr')."')");
      $stmt -> execute(array());
      
      setcookie('login', $log);
      setcookie('password', $password);
      session_start();
      $_SESSION['login']=$log;
      $_SESSION['password']=$password;
      
    } 
    catch(PDOException $e) {
      print('Error: '.$e->getMessage());

      exit();
    }
  }
//print("INSERT INTO bac3_person (name,e_mail,year_of_birth,gender,num_limbs,biografy) VALUES ('".$_POST['name']."','".$_POST['email']."',str_to_date('".$_POST['date']."','%Y-%c-%d'),".$_POST['gender']."-1,".$_POST['numOfLimbs'].",'".$_POST['biography']."')");
//print("INSERT INTO bac5_log (id_person,log,password) VALUES ((select id from bac3_person where name='".$_POST['name']."'),'".$log."','".md5(md5($password.$log).'12937463sdfjbkldfghjb!iu4ytro9huo4938htiuhr')."')");
  setcookie('save', '1',time()+60*60);

  header('Location: index.php');
}
